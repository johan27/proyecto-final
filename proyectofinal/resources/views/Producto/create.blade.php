@extends('layouts.app')
@section('content')
<div class="row">
    <div class="col-lg-12 margin-tb">
        <div class="pull-left">
            <h2>Crear Articulo</h2>
        </div>
        <div class="pull-right">
            <a class="btn btn-primary" href="{{ route('productos.index') }}"> Volver</a>
        </div>
    </div>
</div>
{!! Form::model($tiendas,array('route' => 'productos.store','method'=>'POST')) !!}
@include('Producto.form')
{!! Form::close() !!}
@endsection
