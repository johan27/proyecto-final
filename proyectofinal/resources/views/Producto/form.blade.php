<div class="row">
    <div class="col-xs-12 col-sm-12 col-md-12">
        <div class="form-group">
            <strong>SKU:</strong>
            {!! Form::text('SKU', null, array('placeholder' => 'SKU','class' => 'form-control')) !!}
            @if ($errors->has('SKU'))
            <span class="help-block">
                <strong>{{ $errors->first('SKU') }}</strong>
            </span>
            @endif
        </div>
    </div>
    <div class="col-xs-12 col-sm-12 col-md-12">
        <div class="form-group">
            <strong>nombre:</strong>
            {!! Form::text('nombre', null, array('placeholder' => 'nombre','class' => 'form-control')) !!}
            @if ($errors->has('nombre'))
            <span class="help-block">
                <strong>{{ $errors->first('nombre') }}</strong>
            </span>
            @endif
        </div>
    </div>
    <div class="col-xs-12 col-sm-12 col-md-12">
        <div class="form-group">
            <strong>descripcion:</strong>
            {!! Form::text('descripcion', null, array('placeholder' => 'descripcion','class' => 'form-control')) !!}
            @if ($errors->has('descripcion'))
            <span class="help-block">
                <strong>{{ $errors->first('descripcion') }}</strong>
            </span>
            @endif
        </div>
    </div>
    <div class="col-xs-12 col-sm-12 col-md-12">
        <div class="form-group">
            <strong>imagen:</strong>
            {!! Form::text('imagen', null, array('placeholder' => 'imagen','class' => 'form-control')) !!}
            @if ($errors->has('imagen'))
            <span class="help-block">
                <strong>{{ $errors->first('imagen') }}</strong>
            </span>
            @endif
        </div>
    </div>
    <div class="col-xs-12 col-sm-12 col-md-12">
        <div class="form-group">
            <strong>categorias:</strong>
            {!! Form::text('categorias', null, array('placeholder' => 'categorias','class' => 'form-control')) !!}
            @if ($errors->has('categorias'))
            <span class="help-block">
                <strong>{{ $errors->first('categorias') }}</strong>
            </span>
            @endif
        </div>
    </div>
    <div class="col-xs-12 col-sm-12 col-md-12">
        <div class="form-group">
            <strong>Stock:</strong>
            {!! Form::text('Stock', null, array('placeholder' => 'Stock','class' => 'form-control')) !!}
            @if ($errors->has('Stock'))
            <span class="help-block">
                <strong>{{ $errors->first('Stock') }}</strong>
            </span>
            @endif
        </div>
    </div>
    <div class="col-xs-12 col-sm-12 col-md-12">
        <div class="form-group">
            <strong>precio:</strong>
            {!! Form::text('precio', null, array('placeholder' => 'precio','class' => 'form-control')) !!}
            @if ($errors->has('precio'))
            <span class="help-block">
                <strong>{{ $errors->first('precio') }}</strong>
            </span>
            @endif
        </div>
    </div>
    {!! Form::hidden('user_id', Auth::id() )!!}
    <div class="col-xs-12 col-sm-12 col-md-12 text-center">
        <button type="submit" class="btn btn-primary">Guardar</button>
    </div>
</div>
