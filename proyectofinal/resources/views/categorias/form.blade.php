<div class="row">
  <div class="col-xs-12">
    <div class="form-group">
      <strong>nombre : </strong>
      {!! Form::text('nombre', null, ['placeholder'=>'nombre','class'=>'form-control']) !!}
    </div>
  </div>
  <div class="col-xs-12">
    <a class="btn btn-xs btn-success" href="{{ route('categorias.index') }}">Back</a>
    <button type="submit" class="btn btn-xs btn-primary" name="button">Submit</button>
  </div>
</div>
