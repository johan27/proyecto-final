@extends('layouts.app')
@section('content')
<div class="row">
    <div class="col-lg-12 margin-tb">
        <div class="pull-left">
            <h2>Articulos</h2>
        </div>

    </div>
</div>
@if ($message = Session::get('success'))
<div class="alert alert-success">
    <p>{{ $message }}</p>
</div>
@endif
<table class="table table-bordered">
    <tr>
        <th>No</th>
        <th>SKU</th>
        <th>nombre</th>
        <th>descripcion</th>
        <th>categorias</th>
        <th>Stock</th>
        <th>precio</th>
        <th>imagen</th>

    </tr>
    @foreach ($productos as $producto)
    <tr>
        <td>{{ ++$i }}</td>
        <td>{{ $producto->SKU}}</td>
        <td>{{ $producto->nombre}}</td>
        <td>{{ $producto->descripcion}}</td>
        <td>{{ $producto->categorias}}</td>
        <td>{{ $producto->Stock}}</td>
        <td>{{ $producto->precio}}</td>
        <?php
//aquí coges de donde sea la ruta que quieres mostrar
$ruta = $producto->imagen;
?>
        <td><img src="<?php echo $ruta; ?>" width='238' height='100'>
        </td>

    </tr>
    @endforeach
</table>
{!! $productos->links() !!}
@endsection
