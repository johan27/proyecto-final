<?php
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class Categoriahijas extends Migration
{
    public function up()
    {
        Schema::create('categoriahijas', function (Blueprint $table) {
            $table->increments('id');
            $table->string('nombre');
            $table->string('id_categoria_Padre');
            $table->timestamps();
        });
    }
    public function down()
    {
        Schema::dropIfExists('categoriahijas');
    }
}
