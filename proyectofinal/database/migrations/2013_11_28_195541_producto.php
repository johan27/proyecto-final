<?php
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class Producto extends Migration
{
/**
 * Run the migrations.
 *
 * @return void
 */
    public function up()
    {
        Schema::create('productos', function (Blueprint $table) {
            $table->increments('id');
            $table->string('SKU');
            $table->string('nombre');
            $table->string('descripcion');
            $table->string('imagen');
            $table->string('categorias');
            $table->string('Stock');
            $table->string('precio');
            $table->timestamps();
        });
    }
/**
 * Reverse the migrations.
 *
 * @return void
 */
    public function down()
    {
        Schema::dropIfExists('productos');
    }
}
