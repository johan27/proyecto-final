<?php
namespace App;

use Illuminate\Database\Eloquent\Model;

class categoriahija extends Model
{
    protected $fillable = [
        'nombre', 'id_categoria_Padre',
    ];
    public function user()
    {
        return $this->belongsTo('App\User');
    }
}
