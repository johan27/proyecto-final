<?php
namespace App\Http\Controllers;

use App\categoriahija;
use Illuminate\Http\Request;

class categoriahijas extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    ///llama  a la vista index
    public function index()
    {
        $categorias = categoriahija::paginate(5);
        return view('categoriashija.index', compact('categorias'))
            ->with('i', (request()->input('page', 1) - 1) * 5);
    }
    ///llama a crear categoria
    public function create()
    {
        $tiendas = "";
        return view('categoriashija.create', compact('tiendas'));
    }
    //almacena los datos
    public function store(Request $request)
    {
        $this->validate($request, [
            'nombre' => 'required',
        ]);
        categoriahija::create($request->all());
        return redirect()->route('categoriashija.index')
            ->with('success', 'Producto creado correctamente');
    }
    ///muestra los datos
    public function show($id)
    {
        $categorias = categoriahija::find($id);
        return view('categoriashija.show', compact('categorias'));
    }
    ///llama a la vista editar
    public function edit($id)
    {
        $categorias = categoriahija::find($id);
        return view('categoriashija.edit', compact('categorias'));
    }
    ///proceso para actualizar
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'nombre' => 'required',
        ]);
        categoriahija::find($id)->update($request->all());
        return redirect()->route('categoriashija.index')
            ->with('success', 'producto actualizada correctamente');
    }
    ///proceso  para destruidos
    public function destroy($id)
    {
        categoriahija::find($id)->delete();
        return redirect()->route('categoriashija.index')
            ->with('success', 'categorias eliminada correctamente');
    }
}
