<?php
namespace App\Http\Controllers;

use App\producto;
use App\User;
use Auth;

class HomeController extends Controller
{
/**
 * Create a new controller instance.
 *
 * @return void
 */
    public function __construct()
    {
        $this->middleware('auth');
    }
/**
 * Show the application dashboard.
 *
 * @return \Illuminate\Http\Response
 */
    public function index()
    {
        //busca el tipo de usuario y lo envia a la vista correcta
        $tipo = auth()->user()->Tipo;
        if ($tipo == "user") {

            $productos = producto::paginate(5);
            return view('usuario', compact('productos'))
                ->with('i', (request()->input('page', 1) - 1) * 5);

        } elseif ($tipo == "admin") {
            $user = User::all();
            echo $user[0]->user_count;

            $tama = count($user);

            return view('home', compact('tama'));
        }
    }
}
