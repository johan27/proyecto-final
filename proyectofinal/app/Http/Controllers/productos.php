<?php
namespace App\Http\Controllers;

use App\producto;
use Illuminate\Http\Request;

class productos extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
///llama  a la vista index
    public function index()
    {
        $productos = producto::paginate(5);
        return view('Producto.index', compact('productos'))
            ->with('i', (request()->input('page', 1) - 1) * 5);
    }
///llama a crear categoria
    public function create()
    {
        $tiendas = "";
        return view('Producto.create', compact('tiendas'));
    }
//almacena los datos
    public function store(Request $request)
    {
        $this->validate($request, [
            'nombre'      => 'required',
            'SKU'         => 'required',
            'descripcion' => 'required',
            'imagen'      => 'required',
            'Stock'       => 'required',
            'precio'      => 'required',
        ]);
        producto::create($request->all());
        return redirect()->route('productos.index')
            ->with('success', 'Producto creado correctamente');
    }
///muestra los datos
    public function show($id)
    {
        $producto = producto::find($id);
        return view('Producto.show', compact('producto'));
    }
///llama a la vista editar
    public function edit($id)
    {
        $productos = producto::find($id);
        return view('Producto.edit', compact('productos'));
    }
///proceso para actualizar
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'nombre'      => 'required',
            'SKU'         => 'required',
            'descripcion' => 'required',
            'imagen'      => 'required',
            'Stock'       => 'required',
            'precio'      => 'required',
        ]);
        producto::find($id)->update($request->all());
        return redirect()->route('productos.index')
            ->with('success', 'producto actualizada correctamente');
    }
///proceso  para destruidos
    public function destroy($id)
    {
        producto::find($id)->delete();
        return redirect()->route('productos.index')
            ->with('success', 'producto eliminada correctamente');
    }
}
