<?php
namespace App\Http\Controllers;

use App\categoriaspadre;
use Illuminate\Http\Request;

class categoriaspadres extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    ///llama  a la vista index

    public function index()
    {
        $categorias = categoriaspadre::paginate(5);
        return view('categorias.index', compact('categorias'))
            ->with('i', (request()->input('page', 1) - 1) * 5);
    }
    ///llama a crear categoria

    public function create()
    {
        $tiendas = "";
        return view('categorias.create', compact('tiendas'));
    }
    //almacena los datos

    public function store(Request $request)
    {
        $this->validate($request, [
            'nombre' => 'required',
        ]);
        categoriaspadre::create($request->all());
        return redirect()->route('categorias.index')
            ->with('success', 'Producto creado correctamente');
    }
    ///muestra los datos

    public function show($id)
    {
        $categorias = categoriaspadre::find($id);
        return view('categorias.show', compact('categorias'));
    }
    ///llama a la vista editar

    public function edit($id)
    {
        $categorias = categoriaspadre::find($id);
        return view('categorias.edit', compact('categorias'));
    }
    ///proceso para actualizar

    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'nombre' => 'required',
        ]);
        categoriaspadre::find($id)->update($request->all());
        return redirect()->route('categorias.index')
            ->with('success', 'producto actualizada correctamente');
    }
    ///proceso  para destruidos

    public function destroy($id)
    {
        categoriaspadre::find($id)->delete();
        return redirect()->route('categorias.index')
            ->with('success', 'categorias eliminada correctamente');
    }
}
