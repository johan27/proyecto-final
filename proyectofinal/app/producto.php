<?php
namespace App;

use Illuminate\Database\Eloquent\Model;

class producto extends Model
{
    protected $fillable = [
        'SKU', 'nombre', 'descripcion', 'imagen', 'categorias', 'Stock', 'precio',
    ];
    public function user()
    {
        return $this->belongsTo('App\User');
    }
}
